/*
 * (c) Copyright 2024 Swiss Post Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.post.it.evoting.verifier.backend.verifications.tally.evidence;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Map;

import ch.ech.xmlns.ech_0110._4.Delivery;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingdecrypt.Results;

/**
 * Regroups the input values needed by the VerifyTallyFiles algorithm.
 *
 * <ul>
 *     <li>Election Event Configuration, the configuration-anonymized as {@link Configuration}. Not null.</li>
 *     <li>Tally Control Component Decryptions, the evoting-decrypt as {@link Results}. Not null.</li>
 *     <li>Tally Control Component Results, the eCH-0110 as {@link Delivery}. Not null.</li>
 *     <li>Tally Control Component Detailed Results, the eCH-0222 as {@link ch.ech.xmlns.ech_0222._1.Delivery}. Not null.</li>
 *     <li>L_decodedVotesbb, the list of all selected decoded voting options for each ballot box. Not null.</li>
 * </ul>
 */
public class VerifyTallyFilesInput {

	private final Configuration cantonConfig;
	private final Results tallyComponentDecrypt;
	private final Delivery tallyComponentEch0110;
	private final ch.ech.xmlns.ech_0222._1.Delivery tallyComponentEch0222;
	private final Map<String, TallyComponentVotesPayload> tallyComponentVotesPayloads;

	private VerifyTallyFilesInput(
			final Configuration cantonConfig,
			final Results tallyComponentDecrypt,
			final Delivery tallyComponentEch0110,
			final ch.ech.xmlns.ech_0222._1.Delivery tallyComponentEch0222,
			final Map<String, TallyComponentVotesPayload> tallyComponentVotesPayloads) {
		this.cantonConfig = cantonConfig;
		this.tallyComponentDecrypt = tallyComponentDecrypt;
		this.tallyComponentEch0110 = tallyComponentEch0110;
		this.tallyComponentEch0222 = tallyComponentEch0222;
		this.tallyComponentVotesPayloads = tallyComponentVotesPayloads;
	}

	public Configuration getCantonConfig() {
		return cantonConfig;
	}

	public Results getTallyComponentDecrypt() {
		return tallyComponentDecrypt;
	}

	public Delivery getTallyComponentEch0110() {
		return tallyComponentEch0110;
	}

	public ch.ech.xmlns.ech_0222._1.Delivery getTallyComponentEch0222() {
		return tallyComponentEch0222;
	}

	public final Map<String, TallyComponentVotesPayload> getTallyComponentVotesPayloads() {
		return tallyComponentVotesPayloads;
	}

	public static class Builder {

		private Configuration cantonConfig;
		private Results tallyComponentDecrypt;
		private Delivery tallyComponentEch0110;
		private ch.ech.xmlns.ech_0222._1.Delivery tallyComponentEch0222;
		private Map<String, TallyComponentVotesPayload> tallyComponentVotesPayloads;

		public Builder setCantonConfig(final Configuration cantonConfig) {
			this.cantonConfig = cantonConfig;
			return this;
		}

		public Builder setTallyComponentDecrypt(final Results tallyComponentDecrypt) {
			this.tallyComponentDecrypt = tallyComponentDecrypt;
			return this;
		}

		public Builder setTallyComponentEch0110(final Delivery tallyComponentEch0110) {
			this.tallyComponentEch0110 = tallyComponentEch0110;
			return this;
		}

		public Builder setTallyComponentEch0222(final ch.ech.xmlns.ech_0222._1.Delivery tallyComponentEch0222) {
			this.tallyComponentEch0222 = tallyComponentEch0222;
			return this;
		}

		public Builder setTallyComponentVotesPayloads(final Map<String, TallyComponentVotesPayload> tallyComponentVotesPayloads) {
			this.tallyComponentVotesPayloads = Map.copyOf(tallyComponentVotesPayloads);
			return this;
		}

		public VerifyTallyFilesInput build() {
			checkNotNull(cantonConfig);
			checkNotNull(tallyComponentDecrypt);
			checkNotNull(tallyComponentEch0110);
			checkNotNull(tallyComponentEch0222);
			checkNotNull(tallyComponentVotesPayloads);

			return new VerifyTallyFilesInput(cantonConfig, tallyComponentDecrypt, tallyComponentEch0110, tallyComponentEch0222,
					tallyComponentVotesPayloads);
		}
	}

}
