/*
 * (c) Copyright 2024 Swiss Post Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.post.it.evoting.verifier.backend.verifications.tally.evidence;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.verifier.backend.AbstractVerification;
import ch.post.it.evoting.verifier.backend.Category;
import ch.post.it.evoting.verifier.backend.VerificationDefinition;
import ch.post.it.evoting.verifier.backend.VerificationResult;
import ch.post.it.evoting.verifier.backend.event.TallyEvent;
import ch.post.it.evoting.verifier.backend.processor.ResultPublisherService;
import ch.post.it.evoting.verifier.backend.tools.ElectionDataExtractionService;
import ch.post.it.evoting.verifier.backend.tools.TranslationHelper;
import ch.post.it.evoting.verifier.backend.verifications.tally.TallyVerificationSuite;

@Component
public class VerifyOnlineControlComponents extends AbstractVerification {

	private final ElectionDataExtractionService electionDataExtractionService;
	private final VerifyOnlineControlComponentsAlgorithm verifyOnlineControlComponentsAlgorithm;

	public VerifyOnlineControlComponents(
			final ResultPublisherService resultPublisherService,
			final ElectionDataExtractionService electionDataExtractionService,
			final VerifyOnlineControlComponentsAlgorithm verifyOnlineControlComponentsAlgorithm) {
		super(resultPublisherService);
		this.electionDataExtractionService = electionDataExtractionService;
		this.verifyOnlineControlComponentsAlgorithm = verifyOnlineControlComponentsAlgorithm;
	}

	@Override
	public VerificationDefinition getVerificationDefinition() {
		final VerificationDefinition definition = new VerificationDefinition();
		definition.setBlock(TallyVerificationSuite.BLOCK_NAME);
		definition.setCategory(Category.EVIDENCE);
		definition.setDescription(TranslationHelper.getFromResourceBundle(TallyVerificationSuite.RESOURCE_BUNDLE_NAME,
				"tally.verification1001.description"));
		definition.setId("10.01");
		definition.setName("VerifyOnlineControlComponents");
		definition.addVerifierEvent(TallyEvent.TYPE);
		return definition;
	}

	@Override
	public VerificationResult verify(final Path inputDirectoryPath) {
		final ElectionEventContextPayload electionEventContextPayload = electionDataExtractionService.getElectionEventContextPayload(
				inputDirectoryPath);
		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = electionDataExtractionService.getSetupComponentPublicKeysPayload(
				inputDirectoryPath);
		final VerifyOnlineControlComponentsContext context = new VerifyOnlineControlComponentsContext(electionEventContextPayload,
				setupComponentPublicKeysPayload);

		final Stream<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = electionDataExtractionService.getAllControlComponentBallotBoxPayloadsOrderedByNodeId(
				inputDirectoryPath);
		final Stream<ControlComponentShufflePayload> controlComponentShufflePayloads = electionDataExtractionService.getAllControlComponentShufflePayloadsOrderedByNodeId(
				inputDirectoryPath);
		final Stream<SetupComponentTallyDataPayload> setupComponentTallyDataPayloads = electionDataExtractionService.getSetupComponentTallyDataPayloads(
				inputDirectoryPath);
		final VerifyOnlineControlComponentsInput input = new VerifyOnlineControlComponentsInput(controlComponentBallotBoxPayloads,
				controlComponentShufflePayloads, setupComponentTallyDataPayloads);

		final VerificationResult verificationResult;
		if (verifyOnlineControlComponentsAlgorithm.verifyOnlineControlComponents(context, input)) {
			verificationResult = VerificationResult.success(getVerificationDefinition());
		} else {
			verificationResult = VerificationResult.failure(getVerificationDefinition(),
					TranslationHelper.getFromResourceBundle(TallyVerificationSuite.RESOURCE_BUNDLE_NAME, "tally.verification1001.nok.message"));
		}

		return verificationResult;
	}
}
