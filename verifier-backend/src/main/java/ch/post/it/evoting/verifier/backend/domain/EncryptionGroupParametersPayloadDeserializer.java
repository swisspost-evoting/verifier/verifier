/*
 * (c) Copyright 2024 Swiss Post Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.post.it.evoting.verifier.backend.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EncryptionGroupParametersPayloadDeserializer extends JsonDeserializer<EncryptionGroupParametersPayload> {
	@Override
	public EncryptionGroupParametersPayload deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException {
		final ObjectMapper mapper = new ObjectMapper();

		final JsonNode node = mapper.readTree(parser);
		final JsonNode encryptionGroupNode = node.path("encryptionGroup");

		final EncryptionGroupParameters encryptionGroup = mapper.readValue(encryptionGroupNode.toString(), EncryptionGroupParameters.class);

		return new EncryptionGroupParametersPayload(encryptionGroup);
	}
}
