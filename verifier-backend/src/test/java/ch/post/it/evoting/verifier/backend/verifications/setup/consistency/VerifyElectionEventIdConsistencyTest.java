/*
 * (c) Copyright 2024 Swiss Post Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.post.it.evoting.verifier.backend.verifications.setup.consistency;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.verifier.backend.VerificationResult;
import ch.post.it.evoting.verifier.backend.dataextractors.ControlComponentCodeSharesPayloadDataExtractor;
import ch.post.it.evoting.verifier.backend.dataextractors.ControlComponentPublicKeysPayloadDataExtractor;
import ch.post.it.evoting.verifier.backend.dataextractors.SetupComponentTallyDataPayloadDataExtractor;
import ch.post.it.evoting.verifier.backend.dataextractors.SetupComponentVerificationDataPayloadDataExtractor;
import ch.post.it.evoting.verifier.backend.tools.ElectionDataExtractionService;
import ch.post.it.evoting.verifier.backend.tools.TranslationHelper;
import ch.post.it.evoting.verifier.backend.verifications.setup.SetupVerificationSuite;
import ch.post.it.evoting.verifier.backend.verifications.setup.SetupVerificationTest;

@DisplayName("VerifyElectionEventIdConsistency with")
class VerifyElectionEventIdConsistencyTest extends SetupVerificationTest {

	@BeforeAll
	static void setUpAll() {
		verification = new VerifyElectionEventIdConsistency(resultPublisherServiceMock, electionDataExtractionService);
	}

	@Test
	@DisplayName("valid input files is successful")
	void validInputFiles() {
		final VerificationResult verificationResult = verification.verify(datasetPath);

		final VerificationResult expectedResult = VerificationResult.success(verification.getVerificationDefinition());
		assertEquals(expectedResult, verificationResult);
	}

	@Test
	@DisplayName("inconsistent control component code shares payloads failed")
	void inconsistentControlComponentCodeSharesPayload() {

		final ElectionDataExtractionService extractionServiceSpy = spy(electionDataExtractionService);

		final Stream<ControlComponentCodeSharesPayloadDataExtractor.DataExtraction> dataExtractions = electionDataExtractionService.getAllControlComponentCodeSharesPayloadsDataExtractions(
						datasetPath)
				.map(dataExtraction -> new ControlComponentCodeSharesPayloadDataExtractor.DataExtraction(
								dataExtraction.chunkIds(),
								List.of("wrong election event ID"),
								dataExtraction.nodeIds(),
								dataExtraction.verificationCardSetIds(),
								dataExtraction.verificationCardIdsNode1(),
								dataExtraction.verificationCardIdsNode2(),
								dataExtraction.verificationCardIdsNode3(),
								dataExtraction.verificationCardIdsNode4(),
								dataExtraction.p(),
								dataExtraction.q(),
								dataExtraction.g()
						)
				);

		doReturn(dataExtractions).when(extractionServiceSpy).getAllControlComponentCodeSharesPayloadsDataExtractions(datasetPath);

		final VerifyElectionEventIdConsistency verifyElectionEventIdConsistency = new VerifyElectionEventIdConsistency(
				resultPublisherServiceMock, extractionServiceSpy);

		final VerificationResult result = verifyElectionEventIdConsistency.verify(datasetPath);

		final VerificationResult expectedResult = VerificationResult.failure(verification.getVerificationDefinition(),
				TranslationHelper.getFromResourceBundle(SetupVerificationSuite.RESOURCE_BUNDLE_NAME, "setup.verification309.nok.message"));
		assertEquals(expectedResult, result);
	}

	@Test
	@DisplayName("inconsistent setup component verification data payload failed")
	void inconsistentSetupComponentVerificationDataPayload() {

		final ElectionDataExtractionService extractionServiceSpy = spy(electionDataExtractionService);

		final Stream<SetupComponentVerificationDataPayloadDataExtractor.DataExtraction> dataExtractions = electionDataExtractionService.getAllSetupComponentVerificationDataPayloadsDataExtractions(
						datasetPath)
				.map(dataExtraction -> new SetupComponentVerificationDataPayloadDataExtractor.DataExtraction(
								dataExtraction.chunkId(),
								"wrong election event ID",
								dataExtraction.verificationCardSetId(),
								dataExtraction.verificationCardIds()
						)
				);

		doReturn(dataExtractions).when(extractionServiceSpy).getAllSetupComponentVerificationDataPayloadsDataExtractions(datasetPath);

		final VerifyElectionEventIdConsistency verifyElectionEventIdConsistency = new VerifyElectionEventIdConsistency(
				resultPublisherServiceMock, extractionServiceSpy);

		final VerificationResult result = verifyElectionEventIdConsistency.verify(datasetPath);

		final VerificationResult expectedResult = VerificationResult.failure(verification.getVerificationDefinition(),
				TranslationHelper.getFromResourceBundle(SetupVerificationSuite.RESOURCE_BUNDLE_NAME, "setup.verification309.nok.message"));
		assertEquals(expectedResult, result);
	}

	@Test
	@DisplayName("inconsistent setup component tally data payload failed")
	void inconsistentSetupComponentTallyDataPayload() {

		final ElectionDataExtractionService extractionServiceSpy = spy(electionDataExtractionService);

		final Stream<SetupComponentTallyDataPayloadDataExtractor.DataExtraction> dataExtractions = electionDataExtractionService.getAllSetupComponentTallyDataPayloadsDataExtractions(
						datasetPath)
				.map(dataExtraction -> new SetupComponentTallyDataPayloadDataExtractor.DataExtraction(
								"wrong election event ID",
								dataExtraction.verificationCardSetId(),
								dataExtraction.verificationCardIds()
						)
				);

		doReturn(dataExtractions).when(extractionServiceSpy).getAllSetupComponentTallyDataPayloadsDataExtractions(datasetPath);

		final VerifyElectionEventIdConsistency verifyElectionEventIdConsistency = new VerifyElectionEventIdConsistency(
				resultPublisherServiceMock, extractionServiceSpy);

		final VerificationResult result = verifyElectionEventIdConsistency.verify(datasetPath);

		final VerificationResult expectedResult = VerificationResult.failure(verification.getVerificationDefinition(),
				TranslationHelper.getFromResourceBundle(SetupVerificationSuite.RESOURCE_BUNDLE_NAME, "setup.verification309.nok.message"));
		assertEquals(expectedResult, result);
	}

	@Test
	@DisplayName("inconsistent control component public keys payload failed")
	void inconsistentControlComponentPublicKeysPayload() {

		final ControlComponentPublicKeysPayloadDataExtractor.DataExtraction dataExtraction = new ControlComponentPublicKeysPayloadDataExtractor.DataExtraction(
				0,
				"wrong election event ID"
		);

		final ElectionDataExtractionService extractionServiceSpy = spy(electionDataExtractionService);
		doReturn(Stream.of(dataExtraction)).when(extractionServiceSpy).getControlComponentPublicKeysPayloadsDataExtractions(datasetPath);

		final VerifyElectionEventIdConsistency verifyElectionEventIdConsistency = new VerifyElectionEventIdConsistency(
				resultPublisherServiceMock, extractionServiceSpy);

		final VerificationResult result = verifyElectionEventIdConsistency.verify(datasetPath);

		final VerificationResult expectedResult = VerificationResult.failure(verification.getVerificationDefinition(),
				TranslationHelper.getFromResourceBundle(SetupVerificationSuite.RESOURCE_BUNDLE_NAME, "setup.verification309.nok.message"));
		assertEquals(expectedResult, result);
	}
}
