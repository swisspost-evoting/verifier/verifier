# Verifier Swiss Post

---

Verifier
Copyright 2024 Post CH Ltd.

The verifier implements the verification algorithms of the Swiss Post Voting System developed at Post CH Ltd.

The verifier is E-Voting Community Program Material - please follow our Code of Conduct describing what you can expect from us, the Coordinated Vulnerability Disclosure Policy, and the contributing guidelines.
